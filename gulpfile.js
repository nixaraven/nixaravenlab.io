var gulp = require('gulp'),
    pug = require('gulp-pug')
    sass = require('gulp-sass')
    livereload = require('gulp-livereload')
    googleWebFonts = require('gulp-google-webfonts');

var pug_files = 'src/*.pug';
var layout_files = 'src/layouts/*.pug';
var sass_files = 'src/sass/*.scss';
var output = 'public';
var fontlist = './fonts.list'

function views() {
    return gulp.src(pug_files)
        .pipe(pug())
        .pipe(gulp.dest(output))
        .pipe(livereload());
};

function style(){
    return gulp.src(sass_files)
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest(output+'/css/'))
        .pipe(livereload());
};

function watch(){
    livereload.listen();
    gulp.watch([pug_files, layout_files], gulp.series('views'));
    gulp.watch(sass_files, gulp.series('style'));
};

function icons(){
    return gulp.src('node_modules/@fortawesome/fontawesome-free/webfonts/*')
        .pipe(gulp.dest(output+'/webfonts/'));
};

function fonts(){
    return gulp.src(fontlist)
        .pipe(googleWebFonts({}))
        .pipe(gulp.dest(output+'/webfonts/'))
};

exports.fonts = fonts;
exports.icons = icons;
exports.views = views;
exports.style = style;
exports.watch = watch;
exports.default = gulp.parallel(icons, fonts, views, style);
